# AWS CLI

AWS CLI is a Docker image that contains the AWS CLI suitable for use in CI systems.

## Usage

Consult the [AWS CLI User Guide](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) for
details. If you are using a CI system, you will almost certainly want to configure using environment variables.
See [Environment
Variables to Configure the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html).
